package com.mendalbrot;

/**
 * Created by Kyle on 5/1/2016.
 */
public class Team {
    private String name;
    private int odds;

    private int[][] combos;
    private int comboSize = 0;
    public int getComboSize() { return comboSize; }


    public String getName() { return name; }

    public Team(String n, int o) {
        name = n;
        odds = o;

        combos = new int[odds][4];
    }

    public void addCombo(int[] cb) {
        combos[comboSize] = cb;
        comboSize++;
    }

    public void  removeCombo(int index, int winner) {
        for (int i=0; i<comboSize;i++) {
            if (combos[i][0] != winner && combos[i][1] != winner && combos[i][2] != winner && combos[i][3] != winner) {
                for (int j=i+1;j<comboSize;j++) {
                    combos[j-1] = combos[j];
                }
                comboSize--;
                i--;
            }
        }
    }

}
